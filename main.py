import configparser


def convert():
  mysql = []
  mongo = []
  pgsql = []
  elastic = []
  eptica = []

  config = configparser.ConfigParser(allow_no_value=True)
  config.read('server_groups')
  for section in config.sections():
    print(section)
    for key in config[section]:
      if 'mycbw' in section:
        mysql.append(key)
      elif 'mgp' in section:
        mongo.append(key)
      elif 'pg' in section:
        pgsql.append(key)
      elif 'els_bl' in section:
        elastic.append(key)
      elif 'eptica' in section:
        eptica.append(key)

if __name__ == "__main__":
    convert()